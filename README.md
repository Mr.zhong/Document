参数统一去接口文档找  结合在一起  说明去文档找的 都是没问题的
=====

测试账号：17605035680  、12345678 、  18106079195    密码都是123456
===================================


// 检查手机是否注册  在注册的时候调用一次
------
http://www.jindingyixun.com:8092/verify/telephone

//  发送验证码 参数去文档找
--------
http://www.jindingyixun.com:8092/tigase/smssend 

// 注册  暂时不接 后台没接入
------
http://www.jindingyixun.com:8092/user/register

// 登录接口  参数看文档  提交的密码需要MD5加密 
------
http://www.jindingyixun.com:8092"/user/login
// 忘记密码、修改密码  在医信IM接口文档V3.4
-----------



// 获取就诊者列表   *暂时没有默认就诊者的字段 先不管
-----
http://www.jindingyixun.com:9999/yixinsys/person/getPersonList.action

// 添加就诊者    parenttype 默认1  poputype 默认1001   写死
-----
http://www.jindingyixun.com:9999/yixinsys/person/savePersonInfo.action

// 删除就诊者  看参数
-----
http://www.jindingyixun.com:9999/yixinsys/person/deletePerson.action




// 获取就诊卡列表  参数userid 即可   
------
http://www.jindingyixun.com:9999/yixinsys/card/getMedicalCardList.action

// 添加就诊卡 参数personid 为选择就诊者  UI已经修改   医院选择跳到医院搜索页面  （测试数据  就诊卡号：11857699 就诊人姓名：林禹臣 先添加就诊者再测试添加就诊卡）
-------
http://www.jindingyixun.com:9999/yixinsys/card/saveMedicalCard.action

// 删除就诊卡  
-----
http://www.jindingyixun.com:9999/yixinsys/card/deleteMedicalCard.action

// 搜索医院  orgtype 传1   orgname 为搜索关键字（这个页面在首页模块下有个搜索医院）
-----
http://www.jindingyixun.com:9999/yixinsys/org/getOrgInfo.action?orgname=&orgtype=1




// 获取我的订单（分为药房订单，报告解读，就诊卡充值，视频问诊  四个接口   暂时还没理清楚） 3.12.11
------
http://www.jindingyixun.com:9999/yixinsys/mall/getMallOrder.action  (见接口文档3.12.11） // 药房订单

http://www.jindingyixun.com:9999/yixinsys/org/appoint/patient/getAppointList.action (见接口文档3.8.6） // 预约问诊 
（图片什么都有了 ranks字段 1住院医师，2主治医师，3副主任医师，4主任医师）

http://www.jindingyixun.com:9999/yixinsys/org/getAnalysisList.action  (见接口文档3.8.13） // 报告解读

http://www.jindingyixun.com:9999/yixinsys/query/getRechargeList.action  (见接口文档3.1.6） // 就诊卡充值 
只传userid、page、pagSize

// 我的预约记录
------
http://www.jindingyixun.com:9999/yixinsys/appoint/getAppointList.action   (见接口文档3.2.7）

// 我的收货地址
------
http://www.jindingyixun.com:9999/yixinsys/mall/getAddressList.action  (见接口文档3.12.4）

// 新增收货地址 
------
http://www.jindingyixun.com:9999/yixinsys/mall/saveAddressInfo.action

// 我的钱包 
------
消费记录： http://www.jindingyixun.com:9999/yixinsys/pay/getConsumeList.action
累计消费、可用余额（这个接口除了文档上的参数还需另外带一个walletuserid  这个就传userid）： 
http://www.jindingyixun.com:9999/yixinsys/pay/getWalletInfo.action
// 开通钱包
/yixinsys/pay/saveWalletInfo.action 


// 查询银行卡信息 （API_Base 代表前缀 9999 端口）
-----
#define API_SearchBankInfo API_Base@"/yixinsys/chinapay/getCardBin.action"
// 添加银行卡
-----
#define API_AddBankCard API_Base@"/yixinsys/pay/saveBankCardInfo.action"
// 查询钱包卡信息
-----
#define API_SearchWalletBankInfo API_Base@"/yixinsys/pay/getBankCardInfo.action"
// 充值
---------
#define API_TouchUp API_Base@"/yixinsys/pay/rechargeWallet.action"
// 提现 （这个暂时不接，有点问题）
---------
#define API_WithDraw API_Base@"/yixinsys/chinapay/sinPay.action"

关注模块
========
// 获取关注的机构
------
http://www.jindingyixun.com:9999/yixinsys/org/getMyOrgList.action （见接口文档3.4.10）

// 获取相关机构（目前是传排在第一行机构的id，获取这个机构的相关机构）
--------
http://www.jindingyixun.com:9999/yixinsys/org/getRelationOrgList.action   (见接口文档3.4.15）


药房:
======
获取药械分类
------
http://www.jindingyixun.com:9999/yixinsys/mall/getGoodsTypeInfo.action （见接口文档3.12.12）

获取类型下的药械列表
-------
http://www.jindingyixun.com:9999/yixinsys/org/getOrgList.action   (见接口文档3.4.1）
注：将获取到的机械类型传入本接口中的suborgtype, orgtype设置为药械   suborgtype  传分类返回的orgtypeid
获取药械详情
---------
http://www.jindingyixun.com:9999/yixinsys/getOrgInfo.action   (见接口文档3.4.3）

就医:
======
获取首页数据
-------
http://www.jindingyixun.com:9999/yixinsys/org/getFrontPageInfo.action （待开发）

就医搜索
------
http://www.jindingyixun.com:9999/yixinsys/org/getOrgList.action （见接口文档3.4.1）
或者
http://www.jindingyixun.com:9999/yixinsys/org/getOrgInfo.action （见接口文档3.4.3）
		3.4.3接口带机构聊天系统id

医院详情
------
http://www.jindingyixun.com:9999/yixinsys/org/getOrgInfo.action （见接口文档3.4.3）

科室详情
-------
http://www.jindingyixun.com:9999/yixinsys/org/getDepartmentInfo.action （见接口文档3.4.7）
科室选择（科室列表）
------
http://www.jindingyixun.com:9999/yixinsys/org/getDepartmentList.action （见接口文档3.4.6）

预约问诊医生选择（可预约医生列表）
------
http://www.jindingyixun.com:9999/yixinsys/org/appoint/patient/getAppointDoctorList.action （见接口文档3.8.2）

获取排班
------
http://www.jindingyixun.com:9999/yixinsys/org/appoint/patient/getWorkDoctorPlan.action （见接口文档3.8.14）

预约时间列表(号源)
-------
http://www.jindingyixun.com:9999/yixinsys/org/appoint/patient/getSourceList.action （见接口文档3.8.3）

预约提交
-----
http://www.jindingyixun.com:9999/yixinsys/org/appoint/patient/saveAppoint.action （见接口文档3.8.8）




























